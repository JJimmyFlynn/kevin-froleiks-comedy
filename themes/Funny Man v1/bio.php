<?php
/*
Template Name: Bio
*/
?>

<?php get_header(); ?>
<?php include("parts/twitterAndEvents.php"); ?>
<div class="container">
	<div class="sectionHeading"><span>Bio</span></div>
	<article class="bioBlurb blogPost">
		<?php the_post(); ?>
		<div class="bioPic img"><?php the_post_thumbnail(); ?></div>
		<?php the_content(); ?>

		<a href="/contact" class="btn contact">Get in touch</a>
	</article>
<?php get_footer(); ?>