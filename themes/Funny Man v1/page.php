<?php get_header(); ?>
	<div class="container">
		<article class="blogPost">
		<?php the_post(); ?>
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
		</article>
<?php get_footer(); ?>
