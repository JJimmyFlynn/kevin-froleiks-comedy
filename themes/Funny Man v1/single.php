<?php get_header(); ?>
	<?php include("parts/twitterAndEvents.php"); ?>
	<div class="container">
		<article class="blogPost">
			<?php the_post(); ?>
		
				<h1 class="postTitle"><?php the_title(); ?></h1>
				<p class="pTime"><time><?php the_time(get_option('date_format')); ?></time></p>
				<?php the_content(); ?>
				<?php if(get_field("episode_description")) : ?>
					<p><?php the_field("episode_description"); ?><p>
					<p>Subscribe on <a href="http://www.youtube.com/channel/UCOURi1Vo20MUGPiqtJHHEMg">YouTube</a>
				<?php endif; ?>
		</article>
    <a href="/archives" class="btn olderPosts">Older Posts &rarr;</a>

    <?php comments_template(); ?>

<?php get_footer(); ?>
