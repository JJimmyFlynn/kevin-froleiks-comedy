		<div class="tweets">
			<h3>Latest <span class="twitterColor">Tweet</span></h3>
			<a class="twitter-timeline" data-link-color="#348d24" data-chrome="noheader nofooter noborders transparent" data-tweet-limit="1" href="https://twitter.com/KevinFroleiks" data-widget-id="364073794219945986">Tweets by @KevinFroleiks</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			<a href="https://twitter.com/intent/follow?user_id=159969384" target="blank">Follow Kevin</a>
		</div>

		<div class="gig">
			<h3><span data-icon="2" class="calendar"></span>Next Event</h3>
		<?php 
		$currentDate = date(Ymd);

		$query = new WP_Query(array(
			'post_type' => 'event',
			'meta_query' => array(
					array(
						'key' => 'date',
						'compare' => '>=',
						'value' => $currentDate,
						'type' => 'DATE'
						)
				),
			'meta_key' => 'date',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
			'posts_per_page' => 1
		)); ?>

		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
			<p> Where: <span class="location"><?php the_field("venue"); ?></span>
			<p>When: <span class="dateTime">&nbsp;<?php $date = DateTime::createFromFormat('Ymd', get_field('date'));
				echo $date->format('F jS, Y'); ?> at <?php the_field("time"); ?></span></p>
			<?php endwhile; endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>