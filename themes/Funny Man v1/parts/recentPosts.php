<div class="sectionHeading"><span>Recent Posts</span></div>
			<section class="recentPosts">
				<div class="recentPostsContainer">
					<?php query_posts( 'posts_per_page=3' );?>
				  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<a href="<?php the_permalink(); ?>" class="recentPostModule module">
						<p class="pTime"><time><?php the_time(get_option('date_format')); ?></time></p>
						<h3><?php the_title(); ?></h3>
						<?php the_excerpt(); ?>
					</a>
				  <?php endwhile; endif; ?>
				  <?php wp_reset_query(); ?>

					<a href="/archives" class="btn olderPosts">Older Posts &rarr;</a>
				</div>
			</section>
		</div>