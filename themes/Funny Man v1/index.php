  <?php get_header(); ?>
  <?php include("parts/twitterAndEvents.php"); ?>
    <div class="container">
      <section class="latestPost">
        <div class="sectionHeading"><span>Latest Writing</span></div>
  <?php query_posts( 'posts_per_page=1' );?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="blogPost homepagePost">
          <h1 class="postTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
          <p class="pTime"><time><?php the_time(get_option('date_format')); ?></time></p>
          <?php the_content(); ?>
          <a class="toComments" href="<?php the_permalink(); ?>#disqus_thread">Leave a comment &rarr;</a>
        </article>

  <?php endwhile; endif; ?>
  <?php wp_reset_query(); ?>
      </section>
      <?php include("parts/recentPosts.php"); ?>

  <?php get_footer(); ?>
