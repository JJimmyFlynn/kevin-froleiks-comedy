<?php
/*
Template Name: Events
*/
?>

<?php get_header(); ?>
<div class="container">
	<h1 class="pageTitle">Upcoming Events</h1>

		<?php 
		$currentDate = date(Ymd);

		$query = new WP_Query(array(
			'post_type' => 'event',
			'meta_query' => array(
					array(
						'key' => 'date',
						'compare' => '>=',
						'value' => $currentDate,
						'type' => 'DATE'
						)
				),
			'meta_key' => 'date',
			'orderby' => 'meta_value_num',
			'order' => 'ASC'
		)); ?>
		
	  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
		<article class="event">
			<div class="headings grid col-1-5">
				<p><span data-icon="2" class="heading"> Date:</span></p>
				<p><span data-icon="5" class="heading"> Time:</span></p>
				<p><span data-icon="1" class="heading"> Venue:</span></p>
			</div>

			<div class="eventDetails grid col-2-3">
				<p>
				<?php $date = DateTime::createFromFormat('Ymd', get_field('date'));
				echo $date->format('F jS, Y'); ?>
				</p>
				<p><?php the_field( "time" ); ?></p>
				<p><?php the_field( "venue" ); ?></p>
			<?php if( get_field("venue_address") ): ?>
				<p> <?php the_field("venue_address"); ?></p>
			<?php endif; ?>
			</div>
		</article>
	  <?php endwhile; endif; ?>
		<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>