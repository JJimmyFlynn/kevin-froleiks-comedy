<?php
/*
Template Name: Archive
*/
?>

		<?php get_header(); ?>
		<?php include("parts/twitterAndEvents.php"); ?>
		<div class="container">
		<div class="sectionHeading"><span>Archives</span></div>
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			query_posts('posts_per_page=5&paged=' . $paged); 
		?>
	  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="archiveEntry">
			<p class="pTime"><time><?php the_time(get_option('date_format')); ?></time></p>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
			<div class="excerpt">
			<?php the_excerpt(); ?>
			</div>
			<a class="readOn" href="<?php the_permalink(); ?>">Read More &rarr;</a>
		</article>
		<?php endwhile; endif; ?>

		<div class="pagination">
		<?php previous_posts_link("Newer Posts"); ?>
		<?php next_posts_link("Older Posts"); ?>
		</div>
		
		<?php wp_reset_query(); ?>

		<?php get_footer(); ?>