<?php

// jQuery Insert From Google
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}
//Register Menu
if (function_exists('register_nav_menus')) {
  register_nav_menus(
      array(
          'main_nav' => 'Main Nav'
        )
    );
}
//Change Default Excerpt Length
function custom_excerpt_length( $length ) {
	return 20;
}


//Change Excerpt 'more' String
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//Add Custom Header
add_theme_support( 'custom-header' );

//Add Feature Image
add_theme_support( 'post-thumbnails' );
?>