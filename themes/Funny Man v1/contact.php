<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>
<div class="container">
	<div class="contactBlurb">
		<?php the_content(); ?>
	</div> <!-- end .contactBlurb -->

	<div class="contactForm">
		<?php the_field("contact_form"); ?>
	</div> <!-- end .contactForm -->

<?php get_footer(); ?>