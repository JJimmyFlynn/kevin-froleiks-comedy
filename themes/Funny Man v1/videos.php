<?php
/*
Template Name: Videos
*/
?>

		<?php get_header(); ?>
		<?php include("parts/twitterAndEvents.php"); ?>
		<div class="container">
			<div class="sectionHeading"><span><?php the_title(); ?></span></div>
			<?php $query = new WP_Query(array(
				'post_type' => 'video',
				'show_posts' => -1
			)); ?>

		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
			<section class="videoSection">
			<?php the_content(); ?>
			<?php if (get_field("video_description") ) : ?>
				<div class="videoInfo grid col-2-3 bodyCopy">
					<?php the_field("video_description"); ?>
				</div>
			<?php endif; ?>
			</section>
		  <?php endwhile; endif; ?>
		  <?php wp_reset_postdata(); ?>
		<?php get_footer(); ?>