<?php @header("HTTP/1.1 404 Not found", TRUE, 404); ?>
<?php get_header() ?>
	<div class="container blogPost">
		<p>Oops...This page isn't available, or you've followed a bad link.</p>
		<p>Try again or check back later.</p>

<?php get_footer(); ?>