<?php
/*
Template Name: Feature
*/
?>

<?php get_header(); ?>
<div class="container">
			<section class="latestFeature">
			<h1 class="pageTitle">The Worst Landlord</h1>
			<?php $query = new WP_Query(array(
				'post_type' => 'featured post',
				'posts_per_page' => 1
			)); ?>
		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
			<div class="videoContainer">
				<?php the_content(); ?>
			</div>
			<div class="episodeInfo">
				<div class="grid col-1-2">
					<p class="episodeTitle"><?php the_title(); ?></p>
					<p class="episodeDesc"><?php the_field("episode_description"); ?></p>
					<p>Subscribe on <a href="http://www.youtube.com/channel/UCOURi1Vo20MUGPiqtJHHEMg">YouTube</a>
				</div>
			</div>
		  <?php endwhile; endif; ?>
		  <?php wp_reset_postdata(); ?>
		</section>

		<section class="recentFeatures">
				<div class="sectionHeading"><span>More Episodes</span></div>
				<?php $query = new WP_Query(array(
				'post_type' => 'featured post',
				'posts_per_page' => 3,
				'offset' => 1
				)); ?>
			  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<div class="module featureModule">
					<div class="img grid col-1-3">
						<?php the_post_thumbnail(); ?>
					</div>
					<div>
						<p class="pTime"><?php the_time(get_option('date_format')); ?></p>
						<p class="title"><?php the_title(); ?></p>
						<p class="description"><?php the_field('episode_description'); ?></p>
						<a href="<?php the_permalink(); ?>">Watch now</a>
					</div>
				</div>
				<?php endwhile; endif; ?>
			  <?php wp_reset_postdata(); ?>
			  <div class="btnContain">
				  <a href="http://www.youtube.com/user/TheWorstLandlord" class="btn featureBtn olderPosts">More Episodes</a>
				</div>
		</section>

<?php get_footer(); ?>