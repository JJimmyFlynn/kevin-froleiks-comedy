<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8" />
  <title><?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.ico">
  <!-- stylesheets -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/global.css"/>

  <!-- Typekit Code -->
  <script type="text/javascript">
    (function() {
      var config = {
        kitId: 'ipb6xjt',
        scriptTimeout: 3000
      };
      var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
    })();
  </script>
  <!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-36782995-2', 'kfcomedy.com');
  ga('send', 'pageview');

</script>
  <!-- End Google Analytics -->
  
  <!--WP Generated Header -->
  <?php wp_head(); ?>
  <!--End WP Generated Header -->
  
</head>
<body <?php body_class('class'); ?>> 
<header class="mainHeader">
    <section class="idHeader">
      <h2>Kevin Froleiks<span>Comedian</span></h2>
    </section>

    <div class="social">
      <a href="https://www.facebook.com/kevinfroleikscomedy"><img src="<?php bloginfo('template_url'); ?>/img/facebook.png" alt="Like Kevin on Facebook"></a>
      <a href="https://twitter.com/KevinFroleiks"><img src="<?php bloginfo('template_url'); ?>/img/twitter.png" alt="Follw Kevin on Twitetr"></a>
      <a href="http://www.youtube.com/user/KevinFroleiksComedy"><img src="<?php bloginfo('template_url'); ?>/img/youtube.png" alt="Subscribe to Kevin on Youtube"></a>
      <a href="http://kfcomedy.com/feed/"><img src="<?php bloginfo('template_url'); ?>/img/rss.png" alt="Subscribe to Kevin on Youtube"></a>
    </div> <!-- end. social -->
    <nav role="navigation">
          <?php wp_nav_menu(array('menu' => 'Main Nav',
                                  'container' => 'ul')); ?>
      <div class="nav-menu"><a href="#sidr" id="slideMenu"><span data-icon="3"></span> Menu</a></div>
    </nav>
    <div class="sidr" id="sidr">
          <?php wp_nav_menu(array('menu' => 'Main Nav',
                                  'container' => 'ul')); ?>
    </div> <!-- end .sidr -->
  </header>

  <section class="content" role="main">
  <header role="banner" style="background-image: url(<?php header_image(); ?>)">
    <h1 class="mainBanner screenReader">
      Kevin Froleiks Comedian 
    </h1>
  </header>